import json
import os
import sys
from datetime import datetime as d
command = sys.argv[1]

f = open("list.json","r")
inp = json.load(f)
f.close()

def back_up():
	dest = "."
	for i in range(len(inp)):
		src = str(inp[i][0])
		os.system('cp %s %s' % (src, dest))
	print("-----All Backed Up!-----")
	
def restore():
	dest = "./"
	for i in range(len(inp)):
		src = str(dest + str(inp[i][1]))
		os.system('sudo cp %s %s' % (src, str(inp[i][0])))
	print("-----All Restored Back!-----")	

def git():
	time = d.now().strftime("%H:%M")
	dt = d.now().strftime("%d-%m-%y")
	back_up()
	os.system('git add .')
	os.system('git commit -m "Uploaded at %s on %s"'%(time, dt))
	os.system('git push -u origin master')
	print("-----All Pushed!-----")

if str(command) == "backup":
	back_up()

if str(command) == "restore":
	restore()

if str(command) == "push":
	git()





